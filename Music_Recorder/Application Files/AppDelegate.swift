//
//  AppDelegate.swift
//  Music_Recorder
//
//  Created by Majid Inc on 29/12/2017.
//  Copyright © 2017 Rajad. All rights reserved.
//

import UIKit
import FBSDKCoreKit
import IQKeyboardManagerSwift
import FirebaseCore
import FirebaseDatabase

var userID: String? {
    get{
        return SessionManager.sharedInstance.userID()
    }
}



@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate, GIDSignInDelegate {
    
    override init() {
        //Firebase
        FIRApp.configure()
        
        // keep data locally
      //  FIRDatabase.database().persistenceEnabled = true
        
        // Setup IQKeyboardAvoiding
        IQKeyboardManager.sharedManager().enable = true
        IQKeyboardManager.sharedManager().enableAutoToolbar = false
        
       
        
    }
    
    var window: UIWindow?
    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplicationLaunchOptionsKey: Any]?) -> Bool {
        
        
        
        // Facebook
        FBSDKApplicationDelegate.sharedInstance().application(application, didFinishLaunchingWithOptions: launchOptions)
        
        // Google Sign In
        var configureError: NSError?
        GGLContext.sharedInstance().configureWithError(&configureError)
        if let error = configureError {
            print("We have an error \(error)")
        }
        
        GIDSignIn.sharedInstance().delegate = self
        
       
        
        
        return true
    }
    
    
    // - MARK : Delegate
    func sign(_ signIn: GIDSignIn!, didSignInFor user: GIDGoogleUser!, withError error: Error!) {
        
        if let error = error {
            print("Looks Like we got an error \(error)")
        } else {
            print("Great ! User is connected \(user!)")
            guard let profile = user.profile else { return }
            guard let email = profile.email else { return }
            guard let userID = user.userID else { return }
            let sessionManager = SessionManager.sharedInstance
            guard let name = profile.name else { return }
            sessionManager.invalidate()
            sessionManager.register(email: email, userID: userID)
            UserRepository.default.registre(userId: userID, username: name, mobile: "")
            RootController.default.initialeViewController()
            
        }
    }
    
    
    
    func application(_ app: UIApplication, open url: URL, options: [UIApplicationOpenURLOptionsKey : Any] = [:]) -> Bool {
        
        var handled: Bool = false
        handled = FBSDKApplicationDelegate.sharedInstance().application(app, open: url, options: options)
        if handled { return handled }
        
         handled =  GIDSignIn.sharedInstance().handle(url,
                                                 sourceApplication: options[UIApplicationOpenURLOptionsKey.sourceApplication] as? String,
                                                 annotation: options[UIApplicationOpenURLOptionsKey.annotation])
        
        if handled { return handled }
            
        return false
        
    }
    

    func applicationWillResignActive(_ application: UIApplication) {
        // Sent when the application is about to move from active to inactive state. This can occur for certain types of temporary interruptions (such as an incoming phone call or SMS message) or when the user quits the application and it begins the transition to the background state.
        // Use this method to pause ongoing tasks, disable timers, and invalidate graphics rendering callbacks. Games should use this method to pause the game.
    }

    func applicationDidEnterBackground(_ application: UIApplication) {
        // Use this method to release shared resources, save user data, invalidate timers, and store enough application state information to restore your application to its current state in case it is terminated later.
        // If your application supports background execution, this method is called instead of applicationWillTerminate: when the user quits.
    }

    func applicationWillEnterForeground(_ application: UIApplication) {
        // Called as part of the transition from the background to the active state; here you can undo many of the changes made on entering the background.
    }

    func applicationDidBecomeActive(_ application: UIApplication) {
        // Restart any tasks that were paused (or not yet started) while the application was inactive. If the application was previously in the background, optionally refresh the user interface.
    }

    func applicationWillTerminate(_ application: UIApplication) {
        // Called when the application is about to terminate. Save data if appropriate. See also applicationDidEnterBackground:.
    }


}

