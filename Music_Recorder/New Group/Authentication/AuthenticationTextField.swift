//
//  AuthenticationTextField.swift
//  Music_Recorder
//
//  Created by Majid Inc on 02/01/2018.
//  Copyright © 2018 Rajad. All rights reserved.
//

import Foundation
class AuthenticationTextField: UIStackView, UITextFieldDelegate {
    
    
    lazy var textLabel: UILabel = {self.viewWithTag(TAG.title.rawValue) as! UILabel}()
    lazy var textField: UITextField = { self.viewWithTag(TAG.textField.rawValue) as! UITextField}()
    lazy var separatorView: UIView = {self.viewWithTag(TAG.separator.rawValue)!}()
    lazy var errorLabel: UILabel? = {self.viewWithTag(TAG.error.rawValue) as! UILabel}()
    
    
    override func layoutSubviews() {
        super.layoutSubviews()
        
        let color = #colorLiteral(red: 0.8924263277, green: 0.8924263277, blue: 0.8924263277, alpha: 0.3105193662)
        self.textField.textColor = color
        self.textField.attributedPlaceholder = NSAttributedString(string: self.textField.placeholder!, attributes: [NSAttributedStringKey.foregroundColor : color])
        
    }
    
    override func awakeFromNib() {
        self.textField.delegate = self
    }
    
    var text: String? {
        get{
            return textField.text ?? ""
        } set{
            textField.text = newValue
        }
    }
    private var defaultSeparatorColor = UIColor.white
    // MARK: - Class Methods
    
    func show(state: Status){
        switch state {
        case .normal:
            separatorView.backgroundColor = UIColor.white
            errorLabel?.isHidden = true
        case .error(let error):
            errorLabel?.text = error.userInfo[NSLocalizedDescriptionKey] as? String
            errorLabel?.isHidden = false
            errorLabel?.textColor = #colorLiteral(red: 0.9372549057, green: 0.3490196168, blue: 0.1921568662, alpha: 1)
            separatorView.backgroundColor = #colorLiteral(red: 0.9372549057, green: 0.3490196168, blue: 0.1921568662, alpha: 1)
        }
        
    }
    // MARK: - UITextFieldDelegate protocol methods
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        return true
    }
    func textFieldShouldBeginEditing(_ textField: UITextField) -> Bool {
        UIView.transition(with: textLabel, duration: 0.1, options: .transitionCrossDissolve, animations: {
            self.textLabel.textColor = UIColor.lightGray
        }, completion: nil)
        return true
    }
    
    func textFieldDidEndEditing(_ textField: UITextField, reason: UITextFieldDidEndEditingReason) {
        UIView.transition(with: textLabel, duration: 0.1, options: .transitionCrossDissolve, animations: {
            self.textLabel.textColor = UIColor.white
        }, completion: nil)
    }
    
    enum Status{
        case normal
        case error(NSError)
    }
    
    // MARK: - Associated Types
    private enum TAG: Int{
        case title = 1
        case textField = 2
        case separator = 3
        case error = 4
    }
    
}
