//
//  SignInViewController.swift
//  Music_Recorder
//
//  Created by Majid Inc on 29/12/2017.
//  Copyright © 2017 Rajad. All rights reserved.
//

import UIKit
import FirebaseAuth


class SignInViewController: UIViewController {
    
    @IBOutlet weak var errorMessageLabel: UILabel!
    
    @IBOutlet weak var emailTextField: AuthenticationTextField!
    @IBOutlet weak var passwordTextField: AuthenticationTextField!
    
    @IBOutlet weak var alertView: UIView!
    
    private let keyboardManager = KeyboardHideManager()
    override func viewDidLoad() {
        super.viewDidLoad()
        keyboardManager.targets = [view]
        errorMessageLabel.isHidden = true
        

        // Do any additional setup after loading the view.
    }
    
    
    @IBAction func didTapSignInButton(_ sender: Any) {
        
        self.showSpinner()
        
        guard let email = emailTextField.text?.trimmingCharacters(in: .whitespaces),
             let password = passwordTextField.text?.trimmingCharacters(in: .whitespaces) else {
            return
        }
        
        validateTextEntry(email: email, password: password)
        
        FIRAuth.auth()?.signIn(withEmail: email, password: password, completion: { (user, error) in
            self.errorMessageLabel.isHidden = true
            if let user = user {
               // Authentication
                
                self.hideSpinner()
                
                let email = user.email!
                let uid = user.uid
                SessionManager.sharedInstance.register(email: email, userID: uid)
                RootController.default.initialeViewController()
                print("Good")
                
            }
            if let error = error as NSError? {
                self.hideSpinner()
                self.alert(error: error.localizedDescription)
            }
        })
        
        
        

    }
    
    
    private func alert(error: String) {
        self.errorMessageLabel.isHidden = false
        self.errorMessageLabel.text = error
        self.alertView.backgroundColor = #colorLiteral(red: 0.9254902005, green: 0.2352941185, blue: 0.1019607857, alpha: 1)
    }
    private func normalState(){
        emailTextField.show(state: .normal)
        passwordTextField.show(state: .normal)
    }
    
    private func validateTextEntry(email: String,password: String){
        let textValidation = EntryValidator(email: email, password: password)
        normalState()
        let errors = textValidation.validate(screen: .signIn)
        for error in errors{
            guard let userInfo = error.userInfo["textField"] as? EntryValidator.TextField else { continue }
            switch userInfo{
            case .email: emailTextField.show(state: .error(error))
            case .password: passwordTextField.show(state: .error(error))
            default : continue
            }
        }
    }
    
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
