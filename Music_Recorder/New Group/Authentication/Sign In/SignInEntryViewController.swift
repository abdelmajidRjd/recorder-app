//
//  SignInEntryViewController.swift
//  Music_Recorder
//
//  Created by Majid Inc on 29/12/2017.
//  Copyright © 2017 Rajad. All rights reserved.
//

import UIKit
import FBSDKCoreKit
import FBSDKLoginKit
import AVFoundation

class SignInEntryViewController: UIViewController {
    
    
    private var player: AVPlayer!
    private var playerLayer: AVPlayerLayer!
    
    @IBAction func unwindToSignInEntry(_ segue: UIStoryboardSegue) {
        //
    }

    override func viewDidLoad() {
        super.viewDidLoad()
        
        GIDSignIn.sharedInstance().signOut()
        
        GIDSignIn.sharedInstance().uiDelegate = self
        GIDSignIn.sharedInstance().signInSilently()
        
        guard let url = Bundle.main.url(forResource: "appsplash1", withExtension: "mp4") else { return }
        player = AVPlayer(url: url)
        
        playerLayer = AVPlayerLayer(player: player)
        playerLayer.videoGravity = .resizeAspectFill
        playerLayer.frame = view.layer.frame
        
        player.actionAtItemEnd = .none
        
        player.play()
        
        view.layer.insertSublayer(playerLayer, at: 0)
        
        
        NotificationCenter.default.addObserver(self, selector: #selector(rePlayVideo), name: Notification.Name.AVPlayerItemDidPlayToEndTime, object: nil)
        
        // Do any additional setup after loading the view.
    }
    
    
    
    
    
    
    @objc func rePlayVideo(){
        player.seek(to: kCMTimeZero)
    }
    
        
    
    
    
        
        // Do any additional setup after loading the view.
   

   
    
    
    @IBAction func didGSignButtonWasPressed(_ sender: Any) {
        
        googleLogin()
        
    }
    
    @IBAction func didFSignInButtonWasPressed(_ sender: Any) {
        facebookLogIn()
    }
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
extension SignInEntryViewController: GIDSignInUIDelegate {
}
extension SignInEntryViewController: AuthenticationService {
    func facebookLogIn() {
        FBSDKLoginManager().logIn(withReadPermissions: ["email", "public_profile"], from: self) { (result,error) in
            if let result = result {
                if (result.isCancelled) {
                    print("Canceled ")
                    
                } else {
                    SessionManager.sharedInstance.invalidate()
                    SessionManager.sharedInstance.register(email: "None" , userID: result.token.userID)
                    RootController.default.initialeViewController()
                }
            }
            
            if let _ = error {
                print("You should Handle This Error")
            }
        }
        
    }
    
    func googleLogin() {
        
        GIDSignIn.sharedInstance().signIn()
        
        
    }
}

protocol AuthenticationService : class  {
    func facebookLogIn()
    func googleLogin()
}
