//
//  AuthenticationTabBarViewController.swift
//  Music_Recorder
//
//  Created by Majid Inc on 29/12/2017.
//  Copyright © 2017 Rajad. All rights reserved.
//

import UIKit

class AuthenticationTabController: UITabBarController, UITabBarControllerDelegate {
    
    // MARK: - View Lifecycle
    
    override func viewDidLoad() {
        super.viewDidLoad()
        delegate = self
        // Hide Tab Bar 
        self.tabBar.isHidden = true
    }
    
    // MARK: - UITabBarControllerDelegate Protocol Methods
    
    func tabBarController(_ tabBarController: UITabBarController, animationControllerForTransitionFrom fromVC: UIViewController, to toVC: UIViewController) -> UIViewControllerAnimatedTransitioning? {
        return CrossDissolveAnimator()
    }
    
}
