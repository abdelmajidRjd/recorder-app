//
//  SignUpViewController.swift
//  Music_Recorder
//
//  Created by Majid Inc on 29/12/2017.
//  Copyright © 2017 Rajad. All rights reserved.
//

import UIKit
import FBSDKLoginKit

import AVFoundation

class SignUpEntryViewController: UIViewController {
    
    // - MARK : Properties
    @IBOutlet weak var googleSignInButton: UIButton!
    // - MARK : Player
    private var player: AVPlayer!
    private var playerLayer: AVPlayerLayer!
    
    
    @IBAction func unwindToSignUpEntry(_ segue: UIStoryboardSegue){
        //
    }
    override func viewDidLoad() {
        super.viewDidLoad()
        
        guard let url = Bundle.main.url(forResource: "appsplash1", withExtension: "mp4") else { return }
        player = AVPlayer(url: url)
        
        playerLayer = AVPlayerLayer(player: player)
        playerLayer.videoGravity = .resizeAspectFill
        playerLayer.frame = view.layer.frame
        
        player.actionAtItemEnd = .none
        
        player.play()
        
        view.layer.insertSublayer(playerLayer, at: 0)
        
        
        NotificationCenter.default.addObserver(self, selector: #selector(rePlayVideo), name: Notification.Name.AVPlayerItemDidPlayToEndTime, object: nil)
        
        // Do any additional setup after loading the view.
    }
    
    

   
    @objc func rePlayVideo(){
        player.seek(to: kCMTimeZero)
    }
    

    
    @IBAction func didTapGSignInButton(_ sender: Any) {
        googleLogin()
    }
    
    
    @IBAction func didTapFSignInButton(_ sender: Any) {
        facebookLogIn()
    }
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */
}
extension SignUpEntryViewController: AuthenticationService {
    func facebookLogIn() {
        
        
        
        FBSDKLoginManager().logIn(withReadPermissions: ["email", "public_profile"], from: self) { (result,error) in
            if let result = result {
                if (result.isCancelled) {
                    print("Canceled ")
                    
                } else {
                    
                    SessionManager.sharedInstance.invalidate()
                    SessionManager.sharedInstance.register(email: "None" , userID: result.token.userID)
                    
                    if let user = FBSDKAccessToken.current() {
                        let uid = user.userID!
                            UserRepository.default.registre(userId: uid, username:"", mobile: "")
                    }
                    RootController.default.initialeViewController()
                }
            }
            if let _ = error {
                print("You should Handle This Error")
            }
        }
        
    }
    func googleLogin() {
        GIDSignIn.sharedInstance().signIn()
        
        
        SessionManager.sharedInstance.invalidate()
        
        
        guard let currentUser = GIDSignIn.sharedInstance().currentUser else {return}
        guard let profile = currentUser.profile else { return }
        guard let name = profile.name else { return }
        guard let email = profile.email else { return }
        guard let userId = currentUser.userID else { return }
        SessionManager.sharedInstance.register(email: email, userID: userId)
        UserRepository.default.registre(userId: userId, username: name, mobile: email)
        RootController.default.initialeViewController()
        
    }
}

