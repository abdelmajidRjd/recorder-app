//
//  SignUpViewController.swift
//  Music_Recorder
//
//  Created by Majid Inc on 29/12/2017.
//  Copyright © 2017 Rajad. All rights reserved.
//

import UIKit
import FirebaseAuth

class SignUpViewController: UIViewController {
    
    @IBOutlet weak var errorMessageLabel: UILabel!
    
    @IBOutlet weak var alertView: UIView!
    @IBOutlet weak var userNameTextField: AuthenticationTextField!
    
    @IBOutlet weak var emailTextField: AuthenticationTextField!
    
    @IBOutlet weak var mobileTextField: AuthenticationTextField!
    
    @IBOutlet weak var passwordTextField: AuthenticationTextField!
    
    
    
    
    private let keyboardManager = KeyboardHideManager()
    override func viewDidLoad() {
        super.viewDidLoad()
        keyboardManager.targets = [view]
        
        errorMessageLabel.isHidden = true
        
        // Do any additional setup after loading the view.
    }
    
    

    @IBAction func didTapSignUpButton(_ sender: Any) {
        self.showSpinner()
        guard
            let email = emailTextField.text?.trimmingCharacters(in: .whitespaces),
            let password = passwordTextField.text?.trimmingCharacters(in: .whitespaces),
            let mobile = mobileTextField.text?.trimmingCharacters(in: .whitespaces),
            let name = userNameTextField.text?.trimmingCharacters(in: .whitespaces) else {
                self.hideSpinner()
                return
        }
        EntryValidation(email: email, password: password, mobile: mobile, name: name)
        
        
        FIRAuth.auth()?.createUser(withEmail: email, password: password, completion: { (user, error) in
            
            if let user = user{
                let email = user.email
                let uid = user.uid
                UserRepository.default.registre(userId: uid, username: name, mobile: mobile)
                SessionManager.sharedInstance.register(email: email!, userID: uid)
                self.hideSpinner()
                RootController.default.initialeViewController()
            }
            if let error = error{
                self.hideSpinner()
                self.alert(error: error.localizedDescription)
            }
            
        })
    }
    
    private func alert(error: String) {
        self.errorMessageLabel.isHidden = false
        self.errorMessageLabel.text = error
        self.alertView.backgroundColor = #colorLiteral(red: 0.9254902005, green: 0.2352941185, blue: 0.1019607857, alpha: 1)
    }
    
    private func normalState(){
        emailTextField.show(state: .normal)
        passwordTextField.show(state: .normal)
        userNameTextField.show(state: .normal)
        mobileTextField.show(state: .normal)
    }
    
    private func EntryValidation(email:String, password:String,mobile:String, name: String){
        
       
    self.normalState()
    let errors = EntryValidator(username: name, mobile: mobile, email: email, password: password).validate(screen: .signUp)
    
    

    for error in errors {
        self.hideSpinner()
    guard let userInfo = error.userInfo["textField"] as?  EntryValidator.TextField else { return }
    switch userInfo {
        case .email : emailTextField.show(state: .error(error))
        case .password: passwordTextField.show(state: .error(error))
        case .username: userNameTextField.show(state: .error(error))
        case .mobile : mobileTextField.show(state: .error(error))
        
        }
        
        }
    }
    
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}

