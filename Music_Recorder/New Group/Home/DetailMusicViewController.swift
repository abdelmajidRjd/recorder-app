//
//  DetailMusicViewController.swift
//  Music_Recorder
//
//  Created by Majid Inc on 03/01/2018.
//  Copyright © 2018 Rajad. All rights reserved.
//

import UIKit

class DetailMusicViewController: UIViewController {
    
    
    @IBOutlet weak var playButton: UIButton!
    @IBOutlet weak var ratingView: RatingView!
    @IBOutlet weak var priceLabel: UILabel!
    @IBOutlet weak var musicDurationLabel: UILabel!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        
        
        
    }
    
    @IBAction func didTapGoogleShare(_ sender: Any) {
        share(message: "Check SonGabby it will be Your Music Generation App")
    }
    
    
    @IBAction func didTapFacebookShare(_ sender: Any) {
        share(message: "Facebook ")
    }
    
    @IBAction func didTapYoutubeShareButton(_ sender: Any) {
        share(message: "You Tube ")
    }
    
    @IBAction func didRateExperience(_ sender: Any){
        let button = sender as! UIButton
        let tag = button.tag
        ratingView.rate(tag: tag)
        
    }
    
    @IBAction func didTapPlayButton(_ sender: Any) {
        
    }
    
    private func share(message: String){
        
        let textToShare = [ message ]
        let activityViewController = UIActivityViewController(activityItems: textToShare, applicationActivities: nil)
        
        activityViewController.popoverPresentationController?.sourceView = self.view
        
                activityViewController.excludedActivityTypes = [ UIActivityType.airDrop, UIActivityType.postToFacebook ]
    
        self.present(activityViewController, animated: true, completion: nil)
        
    }
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
