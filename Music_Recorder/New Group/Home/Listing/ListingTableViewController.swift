//
//  ListingTableViewController.swift
//  Music_Recorder
//
//  Created by Majid Inc on 01/01/2018.
//  Copyright © 2018 Rajad. All rights reserved.
//

import UIKit
import FirebaseDatabaseUI
import SwiftyJSON

class ListingTableViewController: UIViewController {
    
    private let LYRICS_ROOT = "lyrics"
    
    @IBOutlet var tableView: UITableView!
    
    private var datasource: FUITableViewDataSource!
    
    private var didFindData:Bool = true
    private var selectedKey: String? = nil
    
    
    
    
    @IBOutlet weak var deleteButton: UIBarButtonItem!
    
    @IBAction func unwindToLyricListing(_ segue: UIStoryboardSegue){
        //
    }
    
    
    override func viewDidLoad() {
        super.viewDidLoad()

        
        self.tableView.delegate = self
        
        
        

        // Uncomment the following line to display an Edit button in the navigation bar for this view controller.
        // self.navigationItem.rightBarButtonItem = self.editButtonItem
        self.tableView.register(ListingTableViewCell())
        
        
        
        guard let uid = userID  else { return }
        let ref = FIRDatabase.database()
            .reference()
            .child(LYRICS_ROOT)
            .child(uid)
        
        let query = ref.queryOrderedByKey()
        
       
        
        self.datasource = self.tableView.bind(to: query, populateCell: { (tableView, indexPath, snapshot) -> UITableViewCell in
            
            
            let data = snapshot.value as? [String:AnyObject] ?? [:]
            
            let json = JSON(data)
            
            let cell = tableView.dequeueReusableCell(indexPath: indexPath) as ListingTableViewCell
            
            do {
                
                let lyric  = try Lyric(JSONString: json.description)
                cell.lyric = lyric
                cell.bind(lyric: lyric, key: snapshot.key)
                
            } catch let error {
                self.show(error: error)
                print(error)
                
            }
            return cell
        })
    
        
    }
    


    @IBAction func didTapAddMessage(_ sender: Any) {
        self.performSegue(withIdentifier: "addLyric", sender: nil)
    }
    
    // MARK: - Table view data source

   
    @IBAction func didTapDeleteButton(_ sender: UIBarButtonItem) {
        
        if sender.isEnabled {
            if let key = selectedKey {
                UserRepository.default.delete(key: key)
            }
        
        }
    }
    
    @IBAction func didTapAddUser(_ sender: UIBarButtonItem) {
        SessionManager.sharedInstance.invalidate()
        RootController.default.initialeViewController()
    }
}

extension ListingTableViewController: UITableViewDelegate {
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        deleteButton.isEnabled = true
        let cell = tableView.cellForRow(at: indexPath) as! ListingTableViewCell
        self.selectedKey = cell.key
        
    }
    
    func tableView(_ tableView: UITableView, editActionsForRowAt indexPath: IndexPath) -> [UITableViewRowAction]? {
        
        deleteButton.isEnabled = false
        let cell = tableView.cellForRow(at: indexPath) as! ListingTableViewCell
        
        cell.swipe()
        
        let recordAction = UITableViewRowAction(style: .default, title: "🎙") { action, index in
            guard let key = cell.key else { return }
            SessionManager.sharedInstance.save(lyric: key)
            self.performSegue(withIdentifier: "recordMusic", sender: nil)
            
            
        }
        let playAction = UITableViewRowAction(style: .default, title: "🎼") { action, index in
            
            self.performSegue(withIdentifier: "showMusicDetail", sender: nil)
        
        }
    
        recordAction.backgroundColor = #colorLiteral(red: 1.0, green: 1.0, blue: 1.0, alpha: 1.0)
        playAction.backgroundColor = #colorLiteral(red: 1.0, green: 1.0, blue: 1.0, alpha: 1.0)
        
        guard let lyric = cell.lyric else { return [recordAction] }
        
        if lyric.recorded {
            return [recordAction,playAction]
        } else {
           return [recordAction]
        }
      
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 100
    }
    
}
