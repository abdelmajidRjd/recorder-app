//
//  ListingTableViewCell.swift
//  Music_Recorder
//
//  Created by Majid Inc on 01/01/2018.
//  Copyright © 2018 Rajad. All rights reserved.
//

import UIKit

class ListingTableViewCell: UITableViewCell, ReuseableView, NibLoadableView {

    public var key: String? = nil
    
    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var descriptionLabel: UILabel!
    @IBOutlet weak var dateLabel: UILabel!
    
    @IBOutlet weak var icon: UIImageView!
    
    public var lyric: Lyric?
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }
    
    override func layoutSubviews() {
        layer.cornerRadius = 5
//        let shadowPath = UIBezierPath(roundedRect: self.bounds, cornerRadius: 5)
//
//        layer.masksToBounds = false
//        layer.shadowColor = #colorLiteral(red: 0.5426246762, green: 0.5426246762, blue: 0.5426246762, alpha: 1)
//        layer.shadowOffset = CGSize(width: 0,   height: 2);
//        layer.shadowOpacity = 0.5
//        layer.shadowPath = shadowPath.cgPath
        super.layoutSubviews()
        
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    func bind(lyric: Lyric, key: String) {
        
        self.key = key
        titleLabel.text = lyric.title
        descriptionLabel.text = lyric.description
        dateLabel.text = lyric.timestamp.dateFromTimestamp()
    }
    func swipe(){
        UIView.animate(withDuration: 0.5, animations: {
            self.icon.alpha = 0.5
        }) { (true) in
            self.icon.alpha = 0.9
        }
    }
    
}
