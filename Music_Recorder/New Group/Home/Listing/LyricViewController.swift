//
//  MessageViewController.swift
//  Music_Recorder
//
//  Created by Majid Inc on 01/01/2018.
//  Copyright © 2018 Rajad. All rights reserved.
//

import UIKit
import IQKeyboardManagerSwift

class LyricViewController: UIViewController {

    //MARK: -  Properties
    @IBOutlet weak var LyricsTextField: UITextField!
    
    @IBOutlet weak var lyricContentTextField: UITextView!
    
    @IBOutlet weak var BarView: UIView!
    
    @IBOutlet weak var ideaButton: UIBarButtonItem!
    
    @IBOutlet weak var bottomConstraint: NSLayoutConstraint!
    
    let keyboardManager = KeyboardHideManager()
    
    private var isTappedBefore: Bool = true
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.LyricsTextField.delegate = self
        
        self.lyricContentTextField.delegate = self
        
        IQKeyboardManager.sharedManager().enable = false
        
        handleKeyboard()
        
        let gestureRecognize = UITapGestureRecognizer(target: self, action: #selector(endEditing))
        self.view.addGestureRecognizer(gestureRecognize)
        
        // Do any additional setup after loading the view.
    }
    
    @objc private func endEditing(){
        
        self.view.endEditing(true)
        UIView.animate(withDuration: 0.5, animations: {
            self.BarView.alpha = 1
        })
        
    }
    
    
    @IBAction func didClickAddLyricButton(_ sender: Any) {
       
    }
    
    //MARK: - Segue
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        
        switch segue.identifier! {
            
        case "showIdea":
            let popOverController = segue.destination as! PopOverIdeaViewController
            popOverController.popoverPresentationController?.delegate = self
            guard let lyric = self.lyricContentTextField.text else { return }
            popOverController.lyric = lyric
        case "showLyricAlert":
            
             let popUpMessageViewController = segue.destination as! PopUpMessageViewController
             
            guard let title = LyricsTextField.text  else {
                popUpMessageViewController.alertDescription = " Please Enter a valid Title "
                return
             }
             
             
            guard let description = lyricContentTextField.text else {
                 popUpMessageViewController.alertDescription = " Please Enter a valid Description "
                return
             }
             if (title.isEmpty || description.isEmpty) {
                popUpMessageViewController.alertDescription = " Please Enter a valid data "
                return
             } else {
                LyricsTextField.text = nil
                lyricContentTextField.text = nil
                UserRepository.default.add(lyrics: title , body: description)
            }
        default:
            print(self)
        }
    }
    
    
   
}


extension LyricViewController {
    
    private func handleKeyboard(){
        
        NotificationCenter.default.addObserver(self, selector: #selector(keyboardEvent), name: NSNotification.Name.UIKeyboardWillShow, object: nil)
        
        NotificationCenter.default.addObserver(self, selector: #selector(keyboardEvent), name: Notification.Name.UIKeyboardWillHide, object: nil)
    }
    
  @objc func keyboardEvent(notification: NSNotification) {
    
    guard  let userInfo = notification.userInfo else{ return }
    let keyboardFrame = userInfo[UIKeyboardFrameEndUserInfoKey] as! CGRect
    let isKeyboardShowing = notification.name == Notification.Name.UIKeyboardWillShow
    let curve = userInfo[UIKeyboardAnimationCurveUserInfoKey] as! NSNumber
    let bottonConstraint =  isKeyboardShowing ? keyboardFrame.size.height : 0.0
    
    bottomConstraint.constant = bottonConstraint
    
    
        UIView.animate(withDuration: 0.1, delay: 0, options: UIViewAnimationOptions(rawValue: UInt(truncating: curve)), animations: {
            self.view.layoutIfNeeded()
            self.view.updateConstraintsIfNeeded()
        
        })
    
    }

    
   
    override func viewWillDisappear(_ animated: Bool) {
        NotificationCenter.default.removeObserver(self, name: Notification.Name.UIKeyboardWillShow, object: nil)
        NotificationCenter.default.removeObserver(self, name: Notification.Name.UIKeyboardWillHide, object: nil)
        super.viewWillDisappear(animated)
        
        if self.isMovingFromParentViewController {
            
            
        }
    }
}

extension LyricViewController: UITextFieldDelegate {
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        return true
    }
    func textFieldDidEndEditing(_ textField: UITextField) {
        UIView.animate(withDuration: 0.6, animations: {
            self.BarView.alpha = 1
        })
    }
}

extension LyricViewController: UITextViewDelegate {
    
    func textViewDidBeginEditing(_ textView: UITextView) {
        
        
        
        
        if self.isTappedBefore {
            textView.text = ""
            self.isTappedBefore = false
        }

        
        UIView.animate(withDuration: 0.6, animations: {
            self.BarView.alpha = 0
        })
        
        
    }
    func textViewDidChange(_ textView: UITextView) {
        guard let text = textView.text else { return }
        if !text.isEmpty {
            self.ideaButton.isEnabled = true
        }else {
            self.ideaButton.isEnabled = false
        }
    }
    
    func textViewDidEndEditing(_ textView: UITextView) {
        self.BarView.isHidden = false
    }
    
}

extension LyricViewController: UIPopoverPresentationControllerDelegate {
    
    func adaptivePresentationStyle(for controller: UIPresentationController) -> UIModalPresentationStyle {
        return UIModalPresentationStyle.none
    }
    
}
