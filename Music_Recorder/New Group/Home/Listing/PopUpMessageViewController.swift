//
//  PopUpMessageViewController.swift
//  Music_Recorder
//
//  Created by Majid Inc on 02/01/2018.
//  Copyright © 2018 Rajad. All rights reserved.
//

import UIKit

class PopUpMessageViewController: UIViewController {

    @IBOutlet weak var textView: UITextView!
    var alertDescription: String? = nil
    
    override func viewDidLoad() {
        super.viewDidLoad()
        guard let description = alertDescription else {return}
        textView.text = description
        
    }

    @IBAction func didTapNextButton(_ sender: Any) {
        dismiss(animated: true)
    }
    
    
    
}
