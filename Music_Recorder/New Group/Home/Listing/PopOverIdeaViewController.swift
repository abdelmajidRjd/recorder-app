//
//  PopOverIdeaViewController.swift
//  Music_Recorder
//
//  Created by Majid Inc on 06/01/2018.
//  Copyright © 2018 Rajad. All rights reserved.
//

import UIKit

class PopOverIdeaViewController: UIViewController {
    
    var lyric: String?
    @IBOutlet weak var suggestionLabel: UILabel!
    
    override func viewDidLoad() {
        super.viewDidLoad()

        
        
        guard let userId = userID,
             let lyric = lyric else {
            self.suggestionLabel.textColor = #colorLiteral(red: 0.7450980544, green: 0.1568627506, blue: 0.07450980693, alpha: 1)
            self.suggestionLabel.text = "Please! Tap something"
            return
        }
        
        MusicService.sharedInstance
            .lyrics(userId: userId , lyrics: lyric)
            .then { suggestion in
                self.suggestionLabel.text = suggestion.list.description
            }
            .catch{ _ in
                self.suggestionLabel.text = "No Suggestion"
                self.suggestionLabel.textColor = #colorLiteral(red: 0.7450980544, green: 0.1568627506, blue: 0.07450980693, alpha: 1)
            }
        // Do any additional setup after loading the view.
    }
    
}
