//
//  ProducerTableViewController.swift
//  Music_Recorder
//
//  Created by Majid Inc on 10/01/2018.
//  Copyright © 2018 Rajad. All rights reserved.
//

import UIKit

class ProducerTableViewController: UIViewController {

    //MARK: - Delegate
    open var delegate: ProducerSelector?
    
    
    @IBOutlet weak var producerTableView: UITableView!
    
    
    private var producers: [Producer]?
    override func viewDidLoad() {
        super.viewDidLoad()
        
    showSpinner()
    self.producerTableView.delegate = self
    self.producerTableView.dataSource = self
        
    self.producerTableView.register(ProducerTableViewCell())
        
        MusicService.sharedInstance.getAvailableProducers(userID: "dd", genre: "dd", lyricsId: "dd").then { (producers) -> Void in
            self.producers = producers
            self.producerTableView.reloadData()
            self.hideSpinner()
            }.catch { error in
            self.show(error: error)
            }
    }
}
extension ProducerTableViewController: UITableViewDelegate{
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        let cell = tableView.cellForRow(at: indexPath) as! ProducerTableViewCell
        
        guard let producer = cell.producer else {return}
        
        delegate?.bind(producer: producer)
        self.dismiss(animated: true)
        
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 100.0
    }
}

extension ProducerTableViewController: UITableViewDataSource {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        guard let producers = producers else { return 0 }
        return producers.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(indexPath: indexPath) as ProducerTableViewCell
        
        let producer = producers![indexPath.row]
        cell.producer = producer
        cell.bind(producer: producer)
        
        return cell
        
    }
    
    
    
    
}
