//
//  ProducerTableViewCell.swift
//  Music_Recorder
//
//  Created by Majid Inc on 10/01/2018.
//  Copyright © 2018 Rajad. All rights reserved.
//

import UIKit
import SDWebImage

class ProducerTableViewCell: UITableViewCell, NibLoadableView, ReuseableView {
    
    @IBOutlet weak var pictureImageView: UIImageView!
    @IBOutlet weak var nameLabel: UILabel!
    @IBOutlet weak var availabilityLabel: UILabel!
    @IBOutlet weak var ratingLabel: UILabel!
    
    var producer: Producer?
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    func bind(producer: Producer) {
        self.nameLabel.text = producer.name
        self.ratingLabel.text = String(producer.rating)
        self.availabilityLabel.text = producer.availability
        
        guard let url = URL(string: producer.pic) else { return }
        pictureImageView.sd_setImage(with: url)
        
    }
    
}
