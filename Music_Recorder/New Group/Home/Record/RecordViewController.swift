//
//  RecordViewController.swift
//  Music_Recorder
//
//  Created by Majid Inc on 03/01/2018.
//  Copyright © 2018 Rajad. All rights reserved.
//

import UIKit
import AVFoundation

protocol ProducerSelector {
    func bind(producer: Producer)
}

class RecordViewController: UIViewController {
    
    
    private var audioFilename: URL!
    
    @IBOutlet weak var recordButton: DefaultButton!
    
    private var audioRecorder: AVAudioRecorder!
    
    private var recordingSession = AVAudioSession.sharedInstance()
    
    private var producer: Producer?
    var player: AVAudioPlayer?
    
    
    // MARK: - Properties Producers
    @IBOutlet weak var producerNameLabel: UILabel!
    @IBOutlet weak var producerNameLLabel: UILabel!
    @IBOutlet weak var producerAvailabilityLabel: UILabel!
    @IBOutlet weak var producerCompanyLabel: UILabel!
    
    // MARK: - Properties Session
    private var musicSession: MusicSession?
    
    
    
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        let tapGesture = UITapGestureRecognizer(target: self, action: #selector(startConference))
        
        recordButton.addGestureRecognizer(tapGesture)
    
        
        showSpinner()
        
        guard let userId = userID else { return }
        
          MusicService.sharedInstance
            
                    .getAvailableProducers(userID: userId, genre: "", lyricsId: "")
            
                    .then { producers in
                        self.updateUI(producer: producers[0])
                        
                    }.catch { error in
                        
                        self.producerCompanyLabel.text = error.localizedDescription
                        
                    }.always {
                        
                self.hideSpinner()
        }
        
    }
    
    
    private func updateUI(producer: Producer) {
        
        self.producer = producer
        
        self.producerNameLabel.text = producer.name
        self.producerNameLLabel.text = producer.name
        self.producerAvailabilityLabel.text = producer.availability
        self.producerCompanyLabel.text = producer.rating.description
    }
  
    
    
    
    @objc private func startConference(){

        guard let userId = userID, let producer = producer else { return }
        
        MusicService.sharedInstance.startSession(userId: userId, lyrics: "", produceId: producer.id).then { musicSession -> Void in
            
            self.musicSession = musicSession
            UserRepository.default.startSession(sessionId: musicSession.id, producer: producer.name)
            
            self.performSegue(withIdentifier: "playMusic", sender: nil)
        }
    }

    
    private func getDirectory() -> URL {
        let paths = FileManager.default.urls(for: .documentDirectory, in: .userDomainMask)
        let documentsDirectory = paths.first!
        return documentsDirectory
    }
    
    func finishRecording(success: Bool) {
        guard let audioRecorder = audioRecorder else { return }
        audioRecorder.stop()
        self.audioRecorder = nil
    }
    
    
   
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        
        guard let identifier = segue.identifier else { return }
        
        switch identifier {
        case "playMusic":
            
            let destination = segue.destination as! playLyricViewController
            
            guard let videoConferenceSession = self.musicSession, let producer = self.producer else {return}
            destination.videoConferenceSession = videoConferenceSession
            destination.producer = producer
            
        case "selectProducer":
            let destination = segue.destination as! ProducerTableViewController
            destination.delegate = self
        default:
            print("Default")
        }
       
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    

}
extension RecordViewController: AVAudioRecorderDelegate {
    
    func audioRecorderDidFinishRecording(_ recorder: AVAudioRecorder, successfully flag: Bool) {
        if !flag {
            finishRecording(success: false)
        }
    }
    
    
}
extension RecordViewController: ProducerSelector {
    
    func bind(producer: Producer){
        self.producer = producer
        
        self.producerNameLabel.text = producer.name
        
        self.producerAvailabilityLabel.text = producer.availability
        self.producerNameLLabel.text = producer.name
    }
}

