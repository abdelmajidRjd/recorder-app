//
//  HomeViewController.swift
//  Music_Recorder
//
//  Created by Majid Inc on 31/12/2017.
//  Copyright © 2017 Rajad. All rights reserved.
//

import UIKit
import FBSDKLoginKit
import FirebaseAuth

class HomeNavigationViewController: UINavigationController {

    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
    
    }
    
    @IBAction func didTapsignOutButton(_ sender: Any) {
        SessionManager.sharedInstance.invalidate()
        GIDSignIn.sharedInstance().signOut()
        do {
            try?  FIRAuth.auth()?.signOut()
        }
        FBSDKLoginManager().logOut()
        RootController.default.initialeViewController()
    }

}

