//
//  playLyricViewController.swift
//  Music_Recorder
//
//  Created by Majid Inc on 03/01/2018.
//  Copyright © 2018 Rajad. All rights reserved.
//

import UIKit
import FBSDKLoginKit
import FirebaseAuth
import AVKit
import SDWebImage
import FirebaseStorage
import FirebaseDatabase

class playLyricViewController: UIViewController {
    
    @IBOutlet weak var lyricLabel: UILabel!
    
    private var recordService = RecorderService()
    
     var videoConferenceSession: MusicSession?
    
    private var player: AVAudioPlayer?
    
    private var data: Data?
    
    var producer: Producer?
    
    @IBOutlet weak var producerImageView: UIImageView!
    
    @IBOutlet weak var playButton: DefaultButton!
    
    private var takeNumber: Int = 1
    
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        
        self.showSpinner()
        
        guard let key = SessionManager.sharedInstance.retreive() else { return }
        
        UserRepository.default.getLyric(key: key) { (lyric) in
            self.hideSpinner()
            self.lyricLabel.text = lyric.description
        }
        
        
        recordService.requestSession { (isPermited) in
            print(isPermited)
        }
        
        
        //guard let sessionId = SessionManager.current else { return }
        guard let session = videoConferenceSession else { return }

        
        guard let producer = producer else { return }
        
        UserRepository.default
            .observe(sessionId: session.id, producer: producer.name)
            .observe(.childChanged) { (snap) in
                
                let isUploaded = snap.value as? Bool ?? false
                
                
                    if isUploaded {
                        // when  Producer Upload the file
                        let _ = UserRepository.default.download(sessionId: session.id, producer: producer.name, completion: { (data, error) in
                            if let data = data {
                                self.data = data
                                self.playMusic()
                                self.recordService.startRecording(number: self.takeNumber)
                            }
                    })
                
                    } else {
                        // when  Producer didn't uploaded it yet
                        
                }
        }
    
    }

    

    @IBAction func didTapPlayButton(_ sender: Any) {
  
        if let player = player {
            if (player.isPlaying) {
                let image = UIImage(named: "ic_play_arrow_48pt")
                self.playButton.setImage(image, for: .normal)
                player.stop()
            } else {
                player.play()
                let image = UIImage(named: "ic_close_48pt")
                self.playButton.setImage(image, for: .normal)
            }
        } else {
            playMusic()
            
        }
    }
    
    @IBAction func didTapSignOut(_ sender: Any) {
        
        SessionManager.sharedInstance.invalidate()
        GIDSignIn.sharedInstance().signOut()
        do {
            try?  FIRAuth.auth()?.signOut()
        }
        FBSDKLoginManager().logOut()
        RootController.default.initialeViewController()
    }

}
extension playLyricViewController {
    
    func playMusic() {
        do {
            try AVAudioSession.sharedInstance().setCategory(AVAudioSessionCategoryPlayback)
            try AVAudioSession.sharedInstance().setActive(true)
            
            /* The following line is required for the player to work on iOS 11. Change the file type accordingly*/
            guard let data = data else { return }
            player = try AVAudioPlayer(data: data,fileTypeHint: AVFileType.mp3.rawValue)
            
            //player = try AVAudioPlayer(contentsOf: URL(fileURLWithPath: ""), fileTypeHint: AVFileType.m4a.rawValue)
            player?.delegate = self
            /* iOS 10 and earlier require the following line:
             player = try AVAudioPlayer(contentsOf: url, fileTypeHint: AVFileTypeMPEGLayer3) */
            
            guard let player = player else { return }
            player.play()
            player.delegate = self
            player.volume = 0.4
            let image = UIImage(named: "ic_close_48pt")
            self.playButton.setImage(image, for: .normal)
            
            
            
        } catch let error {
            print(error.localizedDescription)
        }
    }
    
    private func loadImage(url: String){
        
        let placeholder = UIImage(named: "logo")
        producerImageView.sd_setImage(with: URL(string: url), placeholderImage: placeholder)
    }
    
    
}
extension playLyricViewController: AVAudioPlayerDelegate {
    
    func audioPlayerDidFinishPlaying(_ player: AVAudioPlayer, successfully flag: Bool) {
        
        if flag {
            let image = UIImage(named: "ic_replay_48pt")
            self.playButton.setImage(image, for: .normal)
            recordService.finishRecording(success: flag)
            
            if let recordedFile =  recordService.getRecord(), let producer = self.producer, let session = SessionManager.current, let lyricKey = SessionManager.sharedInstance.retreive() {
                
            
                
                UserRepository.default.save(producer: producer.name , userMail: session.email , record: recordedFile,name: "RECORD"+String(self.takeNumber)+".mp3" )
                
                let segment = String(self.takeNumber)
                
                
                UserRepository.default.updateLyric(key: lyricKey)
                
                MusicService.sharedInstance.saveVoice(sessionId: lyricKey , songSegment: segment, voiceRec: recordedFile).then { success in
                        print(success)
                    }.catch(execute: { (error) in
                        self.show(error: error)
                    })
                
                
            }
            
            
            
            
            self.takeNumber = self.takeNumber + 1
            
            
            
            
        }
    }
    

   
    
    
    
    
}
