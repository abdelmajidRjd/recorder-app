//
//  RootController.swift
//  Music_Recorder
//
//  Created by Majid Inc on 30/12/2017.
//  Copyright © 2017 Rajad. All rights reserved.
//

import UIKit

struct RootController {
    
    static let `default` = RootController()
    private init(){}
    
    func initialeViewController(){
        
        if let _ = SessionManager.current {
            home()
        } else {
            auth()
        }
 
    }
    
    private func home(){
        let homeViewController = StoryManager<HomeNavigationViewController>().instantiate(from: .Home)
        
        switchRootViewController(rootViewController: homeViewController, animated: true, completion: nil)
    }
    
    private func auth(){
        let tabbarController = StoryManager<AuthenticationTabController>().instantiate(from: .Authentication)
        switchRootViewController(rootViewController: tabbarController, animated: true, completion: nil)
        
    }
    
    
    
    
}
extension RootController {
    
    private func switchRootViewController(rootViewController: UIViewController, animated: Bool, completion: (() -> Void)? ) {
        
        let window = UIApplication.shared.delegate?.window!
        window?.backgroundColor = UIColor.white
        if animated {
            UIView.transition(with: window!, duration: 0.5, options: .transitionCrossDissolve, animations: {
                let oldState: Bool = UIView.areAnimationsEnabled
                UIView.setAnimationsEnabled(oldState)
                window!.rootViewController = rootViewController
                UIView.setAnimationsEnabled(oldState)
            }, completion: { (finished: Bool) -> () in
                if (completion != nil) {
                    completion!()
                }
            })
        } else {
            window?.rootViewController = rootViewController
        }
    }
    
}
