//
//  Appearance.swift
//  Music_Recorder
//
//  Created by Majid Inc on 01/01/2018.
//  Copyright © 2018 Rajad. All rights reserved.
//

import Foundation
class Appearance {
    
    static func setup() {
        
        // MARK: - UINavigationBar
        
        let navigationBarAppearance = UINavigationBar.appearance()
        navigationBarAppearance.barTintColor = .white
        navigationBarAppearance.tintColor = #colorLiteral(red: 0.1764705926, green: 0.4980392158, blue: 0.7568627596, alpha: 1)
        navigationBarAppearance.titleTextAttributes = [.font: UIFont.systemFont(ofSize: 17)]
        
    }
    
}
