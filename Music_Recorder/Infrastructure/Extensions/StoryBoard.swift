//
//  UIExtension.swift
//  Music_Recorder
//
//  Created by Majid Inc on 29/12/2017.
//  Copyright © 2017 Rajad. All rights reserved.
//

import UIKit

// MARK: - Storyboard
protocol InstanceFromStoryBoard {
    func instantiate(from name: StoryBoard) -> UIViewController
}
enum StoryBoard: String {
    case Main = "Main"
    case Authentication = "Authentication"
    case Home = "Home"
}
struct StoryManager<T>: InstanceFromStoryBoard where T: UIViewController {
    
     func instantiate(from name: StoryBoard) -> UIViewController {
        
        let identifier = String(describing: T.self)
            return UIStoryboard(name: name.rawValue, bundle: nil).instantiateViewController(withIdentifier: identifier) as! T
            }
}




