//
//  RatingView.swift
//  Music_Recorder
//
//  Created by Majid Inc on 07/01/2018.
//  Copyright © 2018 Rajad. All rights reserved.
//

import UIKit

class RatingView: UIStackView {
    
    private var buttons: [Int:UIButton] = [:]
    
    lazy var one: UIButton =  {self.viewWithTag(TAG.ONE.rawValue) as! UIButton}()
    lazy var two: UIButton =  {self.viewWithTag(TAG.TWO.rawValue) as! UIButton}()
    lazy var three: UIButton = {self.viewWithTag(TAG.THREE.rawValue) as! UIButton}()
    lazy var four: UIButton = {self.viewWithTag(TAG.FOUR.rawValue) as! UIButton}()
    lazy var five: UIButton = {self.viewWithTag(TAG.FIVE.rawValue) as! UIButton}()

    override func awakeFromNib() {
        
        buttons[TAG.ONE.rawValue] = one
        buttons[TAG.TWO.rawValue] = two
        buttons[TAG.THREE.rawValue] = three
        buttons[TAG.FOUR.rawValue] = four
        buttons[TAG.FIVE.rawValue] = five
    }

    
 
    func rate(tag: Int){
        
        for item in 0...tag{
            buttons[item]?.tintColor = #colorLiteral(red: 0.9529411793, green: 0.6862745285, blue: 0.1333333403, alpha: 1)
        }
        for item in tag+1...6{
            buttons[item]?.tintColor = #colorLiteral(red: 0.3643354369, green: 0.542565087, blue: 0.6670984456, alpha: 0.7645796655)
        }
        
    }
    
    

    /*
    // Only override draw() if you perform custom drawing.
    // An empty implementation adversely affects performance during animation.
    override func draw(_ rect: CGRect) {
        // Drawing code
    }
    */

private enum TAG: Int {
    
       case ONE = 1
       case TWO = 2
       case THREE = 3
       case FOUR = 4
       case FIVE = 5
    }
}
