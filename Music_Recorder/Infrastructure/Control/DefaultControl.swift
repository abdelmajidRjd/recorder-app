//
//  DefaultControl.swift
//  Music_Recorder
//
//  Created by Majid Inc on 29/12/2017.
//  Copyright © 2017 Rajad. All rights reserved.
//

import UIKit

// - MARK : Default Button
class DefaultButton: UIButton {
    
    override func layoutSubviews() {
        let layer = self.layer
        let height = self.frame.height
        layer.cornerRadius = height / 2
        super.layoutSubviews()
    }
   
}
// - MARK : Cercle Image
class CercleImage: UIImageView {
    
    override func layoutSubviews() {
        let layer = self.layer
        self.clipsToBounds = true
        let height = self.frame.height
        layer.cornerRadius = height / 2
        super.layoutSubviews()
    }
    
}

// - MARK : Default TextField
@IBDesignable
class DefaultTextField: UITextField {
    
    @IBInspectable var leftImage: UIImage? = UIImage(named: "") {
        didSet {
            updateView()
        }
    }
    override func layoutSubviews() {
        self.leftViewMode = .always
        super.layoutSubviews()
    }
    private func updateView(){
        
        let imageView = UIImageView(image: leftImage)
        imageView.contentMode = .scaleAspectFit
        self.leftView = imageView
        
    }
    
}
// - MARK : Default View
class DefaultView: UIView{
    
    override func layoutSubviews() {
        let layer = self.layer
        layer.cornerRadius = 5
        super.layoutSubviews()
    }
}
// - MARK: RATING VIEW
class ratingStackView: UIStackView {
    
    
    
    
}


