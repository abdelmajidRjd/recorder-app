//
//  Lyric.swift
//  Music_Recorder
//
//  Created by Majid Inc on 02/01/2018.
//  Copyright © 2018 Rajad. All rights reserved.
//

import Foundation
import ObjectMapper
class Lyric: ImmutableMappable{
    
    required init(map: Map) throws {
        self.title = try  map.value("title")
        self.description = try map.value("description")
        self.timestamp = try map.value("timestamp")
        self.recorded = try map.value("recorded")
    }
    
    let title: String
    let description: String
    let timestamp: Int
    let recorded: Bool
}
class Suggestion:ImmutableMappable {
    required init(map: Map) throws {
        self.list = try map.value("suggestion")
    }
    let list: [String]
}

class MusicSession:ImmutableMappable {
    required init(map: Map) throws {
        self.id = try map.value("session_id")
    }
    let id: String
}
