//
//  Producer.swift
//  Music_Recorder
//
//  Created by Majid Inc on 30/12/2017.
//  Copyright © 2017 Rajad. All rights reserved.
//

import Foundation
import ObjectMapper

class Producer: ImmutableMappable {
    let id: String
    let name: String
    let rating: Float
    let availability: String
    let pic: String
    
    required init(map: Map) throws {
        self.id = try map.value("producer_id")
        self.name = try map.value("producer_name")
        self.rating = try map.value("producer_rating")
        self.availability = try map.value("producer_avilability")
        self.pic = try map.value("producer_pic")
    }
}
