//
//  UserRepository.swift
//  Music_Recorder
//
//  Created by Majid Inc on 02/01/2018.
//  Copyright © 2018 Rajad. All rights reserved.
//

import FirebaseDatabase
import FirebaseAuth
import FirebaseStorage
import SwiftyJSON

protocol Repository {
    
    var database: FIRDatabaseReference { get }
    var storage: FIRStorage { get }
    
    func registre(userId: String, username: String, mobile: String)
    func add(lyrics title:String, body:String)
    func delete(key: String)
    func download(sessionId: String,producer name: String, completion: @escaping (Data?,Error?) -> ()) -> FIRStorageDownloadTask?
    
    func startSession(sessionId: String, producer name: String)
    
    func observe(sessionId: String, producer name: String) -> FIRDatabaseReference
    
    func save(producer: String,userMail: String,record: Data, name: String)
    
    func getLyric(key: String, completion:@escaping (Lyric) -> ())
    
    func updateLyric(key: String)
    
}
struct UserRepository: Repository {
    
    func getLyric(key: String, completion:@escaping (Lyric) -> ()){
        
         guard let userId = userID else { return }
        
            database
                .child(LYRICS_ROUTE)
                .child(userId)
                .child(key)
                .observe(.value) { (snap) in
                    
                    let data = snap.value as? [String: AnyObject] ?? [:]
                    let json = JSON(data)
                    
                    do {
                        
                    let lyric = try Lyric(JSONString: json.description)
                        
                    completion(lyric)
                        
                    } catch  {
                        print(error)
                }
            }
    }
    
    func updateLyric(key: String) {
        guard let userId = userID else { return }
        
        database
            .child(LYRICS_ROUTE)
            .child(userId)
            .child(key)
            .updateChildValues(["recorded":true])
        
    }
    
    
    // Childs
    let IsUploaded = "isFileUploaded"
    
    // Firedatabase
    private let USER_ROUTE = "users"
    private let LYRICS_ROUTE = "lyrics"
    private let PRODUCER_SESSION = "producer"
    
    // FireStorage
    private let PRODUCER_FILE = "file.mp3"
    
    
    var database: FIRDatabaseReference = FIRDatabase.database().reference()
    var storage: FIRStorage = FIRStorage.storage()
    
    static let `default` = UserRepository()
    private init(){}
    
    func save(producer: String,userMail: String,record: Data, name: String){
        
        let data = FIRStorageMetadata()
        data.contentType = "audio/mp3"
        
         storage.reference()
            .child(userMail)
            .child(producer)
            .child(name)
            .put(record, metadata: data) { (metadata, error) in
                guard let metadata = metadata else {
                    // Uh-oh, an error occurred!
                    return
                }
                // Metadata contains file metadata such as size, content-type, and download URL.
                print(metadata)
                
        }
        
    }
    
    
    func startSession(sessionId: String, producer name: String) {
        
        guard let session = SessionManager.current else { return }
        
        database
            .child(PRODUCER_SESSION)
            .child(name)
            .child(sessionId)
            .setValue([IsUploaded:false])
        
    
        
        storage.reference()
            .child(session.email)
            .child(sessionId)
            .child(name)
            .child(Date().description)
            .put(Data(), metadata: nil) { (metadata, error) in
                
                
                
        }
        
        
        
        
    }
    
    
    func observe(sessionId: String, producer name: String) -> FIRDatabaseReference {
            return database
                    .child(PRODUCER_SESSION)
                    .child(name)
                    .child(sessionId)
    }

    func download(sessionId: String,producer name: String, completion: @escaping (Data?,Error?) -> ()) -> FIRStorageDownloadTask?{
        
        
        // 20 MB MAX
        let maxData = Int64(20 * 1024 * 1024)
        
        guard let session = SessionManager.current else { return nil }
        
        let downloadTask =  storage.reference()
            .child(session.email)
            .child(sessionId)
            .child(name)
            .child(PRODUCER_FILE)
            
            .data(withMaxSize: maxData) { (data, error) in
                if let data = data {
                    completion(data,error)
                }
                if let error = error {
                    completion(data,error)
                }
        }
        return downloadTask
    }
    
    

    func add(lyrics title: String, body: String) {
        guard let userId = userID else { return }
        
        let autoId = database.childByAutoId().key
        database.child(LYRICS_ROUTE)
               .child(userId)
               .child(autoId)
            .setValue(["title": title,"description":body,"timestamp":FIRServerValue.timestamp(),"recorded":false])
        
    }
    

    
   private func isExist(userId: String,completion: @escaping (_ exist : Bool) -> ()){
        database.child(USER_ROUTE)
            .observe(.value) { (snap) in
                completion(snap.hasChild(userId))
        }
    }
    
    
    
    
    func registre(userId: String,username: String, mobile: String) {
                self.database.child(self.USER_ROUTE)
                    .child(userId)
                    .setValue(["name": username, "mobile": mobile]) { (error, ref) in
                        if let error = error {
                            print(error)
                        }
        }
    }
    
    
    func delete(key: String) {
           database
            .child(LYRICS_ROUTE)
            .child(userID!)
            .child(key)
            .removeValue()
    }
    
}
