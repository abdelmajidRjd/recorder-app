//
//  RecordService.swift
//  Music_Recorder
//
//  Created by Majid Inc on 11/01/2018.
//  Copyright © 2018 Rajad. All rights reserved.
//

import Foundation
import AVFoundation

protocol Recorder {
    func startRecording(number: Int)
    func requestSession(completion: @escaping (Bool) ->())
    func finishRecording(success: Bool)
    
}
class RecorderService: NSObject, Recorder {
    
    
     override init() {
        super.init()
    }
    
    private let EXTENSION = ".m4a"
    private var audioFilename: URL!
    private var audioRecorder: AVAudioRecorder!
    private var recordingSession = AVAudioSession.sharedInstance()
    
    
    private func recordFile(number: Int) -> String {
        return "RECORD"+String(number)+EXTENSION
    }
    
    func requestSession(completion: @escaping (Bool) ->()) {
        
        recordingSession.requestRecordPermission { (hasPermission) in
             completion(hasPermission)
        }
        
    }
    
    func getRecord() -> Data? {
        
        return try? Data(contentsOf: audioFilename, options: .uncached)
    }
    
    func startRecording(number: Int) {
        
        let file = recordFile(number: number)
        self.audioFilename = self.getDirectory().appendingPathComponent(file)
        
        
        let settings = [
            AVFormatIDKey: Int(kAudioFormatMPEG4AAC), // kAudioFormatAppleIMA4, kAudioFormatMPEG4AAC
            AVSampleRateKey: 12000,
            AVNumberOfChannelsKey: 1,
            AVEncoderAudioQualityKey: AVAudioQuality.high.rawValue
        ]
        
        do {
            audioRecorder = try AVAudioRecorder(url: audioFilename, settings: settings)
            audioRecorder.prepareToRecord()
            audioRecorder.delegate = self
            audioRecorder.record()
        } catch {
            print(error.localizedDescription)
            self.finishRecording(success: false)
        }
    }
    
     private func getDirectory() -> URL {
        let paths = FileManager.default.urls(for: .documentDirectory, in: .userDomainMask)
        let documentsDirectory = paths.first!
        return documentsDirectory
    }
    
     func finishRecording(success: Bool) {
        guard let audioRecorder = audioRecorder else { return }
        audioRecorder.stop()
        self.audioRecorder = nil
    }
    
    
}
extension RecorderService: AVAudioRecorderDelegate {
    
    func audioRecorderDidFinishRecording(_ recorder: AVAudioRecorder, successfully flag: Bool) {
        if !flag {
            finishRecording(success: false)
        }
    }
    
}
