//
//  SessionManager.swift
//  Music_Recorder
//
//  Created by Majid Inc on 30/12/2017.
//  Copyright © 2017 Rajad. All rights reserved.
//

import Foundation

fileprivate protocol SessionManagerProtocol {
    
    func register(email: String, userID: String)
    func invalidate()
    
}
struct SessionManager : SessionManagerProtocol {
    
    private static let userKey = "UserInfoKey"
    
    
    static let sharedInstance = SessionManager()
    private init(){}
    
    func register(email: String, userID: String) {
        let session = Session(email: email, userID: userID)
        let data = NSKeyedArchiver.archivedData(withRootObject : session)
        UserDefaults.standard.set(data, forKey: SessionManager.userKey)  // to refactor
    }
    
    func invalidate() {
        UserDefaults.standard.removeObject(forKey: SessionManager.userKey)
    }
    
    func userID() -> String? {
        guard let data = UserDefaults.standard.data(forKey: SessionManager.userKey),
            let session = NSKeyedUnarchiver.unarchiveObject(with: data) as? Session
            else{return nil}
        return session.userID
    }
    static var current: Session? {
        get {
        guard let data = UserDefaults.standard.data(forKey: userKey),
            let session = NSKeyedUnarchiver.unarchiveObject(with: data) as? Session
            else { return nil }
            
        return session
            
        }
        
    }
}
protocol LyricSessionProtocol {
    var LYRIC: String { get }
    func save(lyric key: String)
    func retreive() -> String?
}
extension SessionManager : LyricSessionProtocol {
    var LYRIC: String {
        return "LYRIC"
    }
    
    func save(lyric key: String) {
        UserDefaults.standard.set(key, forKey: LYRIC)
    }
    
    func retreive() -> String? {
        return UserDefaults.standard.string(forKey: LYRIC)
    }
    
    
    
    
}
