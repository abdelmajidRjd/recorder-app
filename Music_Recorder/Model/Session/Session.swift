//
//  Session.swift
//  Music_Recorder
//
//  Created by Majid Inc on 30/12/2017.
//  Copyright © 2017 Rajad. All rights reserved.
//

import Foundation
class Session:NSObject, NSSecureCoding{
    private var _userID = ""
    private var _email = ""

    var userID : String{
        get{
            return _userID
        }
        set{
            _userID = newValue
        }
    }
    var email : String{
        get{
            return _email
        }
        set{
            _email = newValue
        }
    }
    override init() {}
    
    init(email: String,userID: String) {
        self._userID = userID
        self._email = email
    }
    required init?(coder aDecoder: NSCoder) {
        if let authToken = aDecoder.decodeObject(forKey: SessionKeys.userID) as? String{
            _userID = authToken
        }
        if let email = aDecoder.decodeObject(forKey: SessionKeys.email) as? String{
            _email = email
        }
    }
    func encode(with aCoder: NSCoder) {
        aCoder.encode(_userID, forKey : SessionKeys.userID)
        aCoder.encode(_email, forKey : SessionKeys.email)
    }
    // static var supportsSecureCoding = true
    private struct SessionKeys {
        static var userID = "userID"
        static var email = "email"
    }
    public static var supportsSecureCoding = true
}
