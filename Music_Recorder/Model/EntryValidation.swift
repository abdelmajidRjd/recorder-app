//
//  EntryValidation.swift
//  Music_Recorder
//
//  Created by Majid Inc on 31/12/2017.
//  Copyright © 2017 Rajad. All rights reserved.
//

import Foundation
struct EntryValidator {

    let email: String?
    let password: String?
    let username: String?
    let mobile: String?

    init(username: String? = nil, mobile: String? = nil, email: String, password: String) {
        self.email = email
        self.password = password
        self.mobile = mobile
        self.username = username
    }
    
     func validate(email: String?) -> Bool{
        guard let email = email else { return false}
        let emailRegEx = "[A-Z0-9a-z._%+-]+@[A-Za-z0-9.-]+\\.[A-Za-z]{2,}"
        let emailTest = NSPredicate(format:"SELF MATCHES %@", emailRegEx)
        return emailTest.evaluate(with: email)
    }
    
     func validate(password: String?) -> Bool{
        guard let password = password else { return false}
        let passwordRegEx = "[A-Za-z0-9]{6,}"
        let passwordTest = NSPredicate(format:"SELF MATCHES %@", passwordRegEx)
        return passwordTest.evaluate(with: password)
    }
    func validate(name: String?) -> Bool {
        guard let name = name else { return false}
        let nameRegEx = "[A-Za-z]{2,}"
        let nameTest = NSPredicate(format:"SELF MATCHES %@", nameRegEx)
        return nameTest.evaluate(with: name)
    }
    func validate(mobile: String?) -> Bool {
        guard let mobile = mobile else { return false}
        //let PHONE_REGEX = "^\\d{3}-\\d{3}-\\d{4}$"
        let PHONE_REGEX =  "[A-Za-z0-9]{7,}"
        let phoneTest = NSPredicate(format: "SELF MATCHES %@", PHONE_REGEX)
        let result =  phoneTest.evaluate(with: mobile)
        return result
    }
    
    func error(field: TextField, reason: String) -> NSError{
        return NSError(domain: "", code: -1, userInfo: [NSLocalizedDescriptionKey:reason,"textField":field])
    }
    func validate(screen: Screen) -> [NSError]{
        var errors: [NSError] = []
        if !validate(email:email) {
            errors.append(error(field: .email, reason: "invalid email"))
        }
        if !validate(password: password){
            errors.append(error(field: .password, reason: "You must enter at least 6 digits"))
        }
        if screen == .signUp {
            if !validate(name: username){
                errors.append(error(field: .username, reason: "Your name should not have numbers & specicial chars"))
            }
            if !validate(mobile: mobile){
                errors.append(error(field: .mobile, reason: "invalid mobile phone"))
            }
        }
        return errors
    }
    enum Screen{
        case signIn,signUp
    }
    enum TextField{
        case email,password,username,mobile
    }
}
