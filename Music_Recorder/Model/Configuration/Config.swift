//
//  AppConfig.swift
//  Music_Recorder
//
//  Created by Majid Inc on 30/12/2017.
//  Copyright © 2017 Rajad. All rights reserved.
//

import Foundation
import SwiftyJSON
 
 struct Configuration {
 
 fileprivate let data: JSON

    // MARK: - Init Method
 init() {
    let filePath = Bundle.main.path(forResource: "Config", ofType: "plist") ?? ""
    let data = NSDictionary(contentsOfFile: filePath) ?? [:]
    let json = JSON(data)
    self.data = json
 }
}
 extension Configuration {
 
 var baseURL: URL {
 return URL(string: data["baseURL"].stringValue)!
    }
 
 }

