//
//  NetworkManager.swift
//  Music_Recorder
//
//  Created by Majid Inc on 29/12/2017.
//  Copyright © 2017 Rajad. All rights reserved.
//

import Foundation
import Moya
import PromiseKit
import SwiftyJSON

struct NetworkManager {
    
    typealias  Provider = MoyaProvider<MusicRecorderAPI>
    
    @discardableResult
    static func request(target: MusicRecorderAPI) -> Promise<JSON> {
        
        let (promise, fulfill, reject) = Promise<JSON>.pending()
        
        
        let provider = Provider(stubClosure: MoyaProvider.delayedStub(1), plugins: target.plugins)
        
        provider.request(target) { result in
            
            switch result {
            case .success(let response):
                
                let json = try? JSON(data: response.data)
                
                fulfill(json!)
                
            case .failure(let error): reject(error)
            }
            
        }
        
        return promise
        
    }
    
    static func sampleRequest(target: MusicRecorderAPI) -> Promise<String> {
        let (promise, fulfill, reject) = Promise<String>.pending()
        
        let provider = Provider(plugins: target.plugins)
        
        provider.request(target) { result in
            
            switch result {
            case .success(let response):
                
                let success =  String(data: response.data, encoding: String.Encoding.utf8)!
                
                fulfill(success)
                
            case .failure(let error): reject(error)
            }
            
        }
        return promise
    }
    
}
