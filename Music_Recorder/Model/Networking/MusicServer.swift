//
//  MusicServer.swift
//  Music_Recorder
//
//  Created by Majid Inc on 02/01/2018.
//  Copyright © 2018 Rajad. All rights reserved.
//

import Foundation
import PromiseKit
import SwiftyJSON
import ObjectMapper

protocol MusicStore {
    
    func lyrics(userId: String, lyrics: String) -> Promise<Suggestion>
    
    func getAvailableProducers(userID: String, genre: String, lyricsId: String) -> Promise<[Producer]>
    
    func startSession(userId: String, lyrics: String, produceId: String) -> Promise<MusicSession>
    
    func saveVoice(sessionId: String, songSegment: String, voiceRec: Data) -> Promise<String>
    
}
struct MusicService: MusicStore {
    
    static let sharedInstance = MusicService()
    private init(){}
    
    func saveVoice(sessionId: String, songSegment: String, voiceRec: Data) -> Promise<String> {
    
        let promise = NetworkManager.sampleRequest(target: .saveVoice(sessionId: sessionId, songSegment: songSegment, voiceRec: voiceRec))
        return promise.then { response -> Promise<String> in
            return Promise<String>(value: response)
        }
      
    }
    
    
    func startSession(userId: String, lyrics: String, produceId: String) -> Promise<MusicSession> {
        
        let promise = NetworkManager.request(target: .startSession(userId: userId, lyrics: lyrics, produceId: produceId))
        
        return promise.then {  json -> Promise<MusicSession> in
            return Promise<MusicSession>(value: try MusicSession(json: json))
        }
    }
    
    
    func getAvailableProducers(userID: String, genre: String, lyricsId: String) -> Promise<[Producer]> {
        let promise = NetworkManager.request(target: .getAvailableProducers(userID: userID, genre: genre, lyricsId: lyricsId))
        return promise.then { json -> Promise<[Producer]> in
            return Promise<[Producer]>(value: try Producer.collection(from: json))
        }
    }
  
     
    func lyrics(userId: String, lyrics: String) ->Promise<Suggestion> {

        let promise = NetworkManager.request(target: .getLyricsIdea(userID: userId, lyrics: lyrics))

        return promise.then { json -> Promise<Suggestion> in
            let suggestion = Promise<Suggestion>(value: try Suggestion(json: json))
            return suggestion

        }

    }
    
    
}
