//
//  MusicRecorderAPI.swift
//  Music_Recorder
//
//  Created by Majid Inc on 29/12/2017.
//  Copyright © 2017 Rajad. All rights reserved.
//

import Foundation
import Moya

enum MusicRecorderAPI {
    case getLyricsIdea(userID: String, lyrics: String)
    case getAvailableProducers(userID: String, genre: String, lyricsId: String)
    case startSession(userId: String, lyrics: String, produceId: String)
    case saveVoice(sessionId: String, songSegment: String, voiceRec: Data)
}

extension MusicRecorderAPI: TargetType {

    var baseURL: URL { return Configuration().baseURL }

    var path: String {
        
        
        switch self {
        case .getAvailableProducers: return "/get_available_producers"
        case .getLyricsIdea : return "/get_lyric_ideas"
        case .saveVoice : return "/save_voice"
        case .startSession: return "/start_session"
        }
        
    }

    var method: Moya.Method {
        return .post
    }

    var sampleData: Data {
        
        switch self {
        case .getAvailableProducers:
            guard let url = Bundle.main.url(forResource: "available_producers", withExtension: "json") else { return Data()}
            return try! Data(contentsOf: url, options: .alwaysMapped)
        case .startSession:
            guard let url = Bundle.main.url(forResource: "session", withExtension: "json") else { return Data()}
            return try! Data(contentsOf: url, options: .alwaysMapped)
            
        case .getLyricsIdea:
            
            guard let url = Bundle.main.url(forResource: "suggestion", withExtension: "json") else { return Data()}
            
            return try! Data(contentsOf: url, options: .alwaysMapped)
            
        default:
                return Data()
        }
        
    }

    var task: Task {
        if let params = parameters {
            
            return .requestParameters(parameters: params, encoding: encoding)
        }
        return .requestPlain
    }

    var headers: [String: String]? {
        return nil
    }

    var validate: Bool {
        return true
    }

    // MARK: Internals

    private var encoding: ParameterEncoding {
        
        switch self {
        case .getAvailableProducers:  return JSONEncoding.default
        default: return URLEncoding.default
        }
    }

    private var parameters: [String: Any]? {
        
        switch self {
            
        case .getAvailableProducers(let userId,let genre,let lyricsId):
            return ["userid": userId,"genre": genre, "lyricsid": lyricsId]
        case .getLyricsIdea(let userid,let  lyrics):
            return ["userid": userid,"lyrics": lyrics]
        case .saveVoice(let sessionId,let songSegment,_):
            return ["sessionid": sessionId,"songsegment": songSegment]
        case .startSession(let userId,let lyrics,let produceId):
            return ["userid": userId,"lyricsid": lyrics,"producerid": produceId]
        }

    }
    
    
    
    /*
     var multipartBody: [MultipartFormData]? {
     switch self {
     case .PostTest(let multipartData):
     
     guard let image = multipartData["image"] as? [NSData] else { return[] }
     
     let formData: [MultipartFormData] = image.map{MultipartFormData(provider: .Data($0), name: "images", mimeType: "image/jpeg", fileName: "photo.jpg")}
     return formData
     
     
     default:
     return []
     }
     }
    */

}

