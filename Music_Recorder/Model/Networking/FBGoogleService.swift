//
//  FBGoogleService.swift
//  Music_Recorder
//
//  Created by Majid Inc on 06/01/2018.
//  Copyright © 2018 Rajad. All rights reserved.
//

import Foundation
import FBSDKCoreKit

struct FacebookService {
    
    func get(completion: @escaping (String,String,Error?) -> ()) {
        
        if let _ = FBSDKAccessToken.current() {
            let request = FBSDKGraphRequest(graphPath: "me", parameters: ["fields":"name,email"])
            
          let _ =  request?.start(completionHandler: { (_, result, error) in
                if let error = error as NSError? {
                   print(error.localizedDescription)
                } else {
                    if let result = result as? [String: String]{
                        let (email, name) = (result["name"]!, result["email"]!)
                        completion(email, name,error)
                    }
                   
                }
            })
        
        }
        
   
        
        
        
    }
    
}

