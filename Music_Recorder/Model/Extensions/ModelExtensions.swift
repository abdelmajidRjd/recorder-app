//
//  ModelExtensions.swift
//  Music_Recorder
//
//  Created by Majid Inc on 29/12/2017.
//  Copyright © 2017 Rajad. All rights reserved.
//

import Foundation
import SwiftyJSON
import PromiseKit
import ObjectMapper
import Moya

protocol Mouse {
    var color: UIColor { get }
    func rightClick()
}

struct Sourie: Mouse {
    var color: UIColor = #colorLiteral(red: 1, green: 0.1608447924, blue: 0.3274911294, alpha: 1)
}

extension Mouse {
    func rightClick() {
        print("Default Imple")
    }
}

extension TargetType {
    var plugins : [PluginType] {
        var plugins = [PluginType]()
        plugins.insert(NetworkLoggerPlugin(), at: 0)
        return plugins
    }
}
extension ImmutableMappable {
    
    init(json: JSON) throws {
        try self.init(JSONString: json.description)
    }
    
    static func collection(from json: JSON) throws -> [Self] {
        
        var objects = [Self]()
        
        guard let array = json.array else {
            let _ = try Self.init(json: JSON([:])) // Trigger exception
            return []
        }
        
        for json in array {
            objects.append(try Self.init(json: json))
        }
        return objects
    }
}
extension Date {
    
    func month() -> String {
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "MMM"
        let strMonth = dateFormatter.string(from: self)
        return strMonth
    }
}


// MARK: - TIMESTAMP
extension Int {
    func dateFromTimestamp() -> String {
        let date = Date(timeIntervalSince1970: Double(self) / 1000)
        let dateFormatter = DateFormatter()
        dateFormatter.timeZone = TimeZone(abbreviation: "GMT") //Set timezone that you want
        dateFormatter.locale = NSLocale.current
        
        dateFormatter.dateFormat = " dd, yyyy" //Specify your format that you want
        return date.month()+dateFormatter.string(from: date)
    }
}

